Užitečné odkazy
===============

- Můj repozitář: <https://gitlab.com/dlukes/pyshiny/>
- Statická verze aplikace na GitLab Pages (automaticky vytvořená pomocí `shinylive
  export` v GitLab CI při každé pushnuté změně): <https://dlukes.gitlab.io/pyshiny/>.
  Obecný formát je `https://UŽIVATELSKÉ_JMÉNO.gitlab.io/JMÉNO_REPOZITÁŘE/`.
- Aplikace včetně editoru, který umožňuje ji za běhu interaktivně upravovat (je
  defaultně k dispozici vždy při použití `shinylive export`; stačí k URL přidat ještě
  `/edit/`): <https://dlukes.gitlab.io/pyshiny/edit/>

- Shiny:
  - Shiny for Python: <https://shiny.rstudio.com/py/>
  - Shinylive: <https://shiny.rstudio.com/py/docs/shinylive.html>
  - Galerie s inspirací, co všechno Shiny umí:
    - <https://shiny.rstudio.com/py/gallery/>
    - <https://shinylive.io/py/examples/>

- Nasazení statické verze aplikace (exportované pomocí Shinylive):
  - GitLab Pages (vyžaduje zadání platební karty):
    - základní přehled: <https://about.gitlab.com/stages-devops-lifecycle/pages/>
    - tutorial: <https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_ui.html>
  - Netlify (v základní podobě nevyžaduje ani registraci, ale aplikace je pak dostupná
    pouze 1 hodinu): <https://app.netlify.com/drop/>
  - Lze pochopitelně použít i libovolné jiné statické hostovací služby, např.
    <https://webzdarma.cz/> apod.

Varianty nasazení aplikace
==========================

1. **GitLab** Výhody: export není potřeba dělat při každé změně ručně na Jupyteru, stačí
   změny pushnout do GitLabu a export se sám zaktualizuje a nasadí.

   Postup: nemáte-li účet na GitLabu ověřený pomocí platební karty, učiňte tak. Pak
   přepište obsah souboru `.gitlab-ci.yml`
   [tímhle](https://gitlab.com/dlukes/shiny-lexical-dispersion/-/blob/master/.gitlab-ci.yml)
   a smažte ručně vytvořený podadresář `public` (máte-li). Commitněte, pushněte na
   GitLab, po chvíli by měl automatický export doběhnout a výsledek by měl být dostupný
   na adrese `UŽIVATELSKÉ_JMÉNO.gitlab.io/JMÉNO_REPOZITÁŘE/`. Víc o funkcionalitě GitLab
   Pages v [tomto přehledu](https://about.gitlab.com/stages-devops-lifecycle/pages/)
   nebo v [detailní dokumentaci](https://docs.gitlab.com/ee/user/project/pages/).

   Nevýhody: automatický export probíhá v rámci služby [GitLab
   CI/CD](https://docs.gitlab.com/ee/ci/), která je sice zdarma, ale její využití
   vyžaduje zadání platebních údajů v rámci vašeho GitLabového účtu. Navíc si možná v
   tomhle projektu nechcete komplikovat život gitem ani GitLabem, stačí vám Shiny a jen
   byste rádi výslednou aplikaci někam vystavili. Pro tyhle případy je tu varianta 2...

2. **Netlify** Postup: na Jupyteru stačí udělat ruční export aplikace v terminálu pomocí
   příkazu `shinylive export . public`. Dejte si pozor, abyste se při spuštění tohoto
   příkazu nacházeli v adresáři aplikace -- když spustíte `ls`, měl by se vám ukázat
   soubor `app.py` -- případně se tam nejdřív přesuňte pomocí příkazu `cd
   CESTA/K/ADRESÁŘI/S/APLIKACÍ`.

   Příkazem `shinylive export . public` vznikne adresář `public/`, na který můžete v
   levé svislém podokně s prohlížečem souborů kliknout pravým tlačítkem myši, zvolit
   *Download as an Archive* a stáhnout si soubor `public.zip`. Ten pak stačí přetáhnout
   na stránku <https://app.netlify.com/drop> a aplikace by měla být za chvíli nasazená.

   Jde to dokonce bez registrace, ale v takovém případě nelze změnit automaticky
   vygenerovanou subdoménu (část adresy před první tečkou) a aplikace se po 1 hodině
   zase smaže. Takže bych doporučoval se zaregistrovat (žádnou platební kartu to po mně
   nechtělo).

Zápočet
=======

Připomínám též dvě možnosti závěrečného úkolu, za který můžete získat zápočet:

1. Vyvinete/upravíte Shiny aplikaci podle vlastního nápadu či zájmu, či čistě na základě
   inspirace existujícími příklady ([zde](https://shiny.rstudio.com/py/gallery/) a
   [zde](https://shinylive.io/py/examples/)). Pokud to půjde, tak ji i nasadíte pomocí
   Shinylive (viz popsané dvě možnosti výše) a pošlete mi odkaz společně s cestou na
   Jupyteru, kde máte zdrojový kód. Není potřeba, aby to bylo něco extra
   sofistikovaného, stačí i relativně drobné změny oproti zmíněným příkladům. Takové
   dobré pravidlo pravé ruky: až budete mít něco, s čím byste se rádi pochlubili blízkým
   či kamarádům, tak je to určitě více než dostačující pro získání zápočtu :)
2. Pro ty, kdo by se Shiny raději vyhnuli, je v adresáři `edu/python/ls22-23` pracovní
   list `2-99-final.ipynb` na téma topic modeling. Tohle je tradičněji pojatý úkol ve
   stejném duchu jako závěrečný úkol ze zimního semestru.

Pro získání zápočtu stačí splnit jen **jednu** z uvedených možností.
