from pathlib import Path

# If you take a look at the source code of nltk.draw.dispersion_plot (e.g. in a Jupyter
# notebook with ??nltk.draw.dispersion_plot, or the current development version on
# GitHub at <https://github.com/nltk/nltk/blob/develop/nltk/draw/dispersion.py>), you'll
# see that Matplotlib -- the actual plotting library that does all the heavy lifting --
# is only imported within the function, not in the toplevel scope of the module. This
# makes it impossible for `shinylive export` to figure out that Matplotlib should be
# included as a dependency in the exported app bundle. So we need to give it a hint that
# we need it by explicitly importing it in app.py.
import matplotlib, matplotlib.ticker
import nltk
from shiny import App, reactive, render, ui

# We need to help NLTK find the punkt data files we download as part of the app bundle
# build process in .gitlab-ci.yml. In Shinylive, all files included in the exported app
# bundle are put in app.json. From inside the running app, they are then accessible
# under a subdirectory (of the current working directory) which starts with an app_*
# prefix. The nltk_data folder, which we create in .gitlab-ci.yml, is there too.
#
# If you go to the editable version of the app <https://dlukes.gitlab.io/pyshiny/edit/>,
# you'll see all of the bundled files opened as tabs in the source code editor. Or you
# can get a list of them by evaluating the following code in the Python REPL below the
# editor:
#
# >>> from pathlib import Path
# >>> for p in sorted(Path().resolve().rglob("*")):
# ...     print(p)
nltk.data.path.extend(Path().resolve().glob("app_*/nltk_data"))

# Since we're including punkt models for all available languages in the app bundle (not
# just English, which nltk.word_tokenize uses by default), and therefore forcing users
# to download them, we might as well allow them to pick which one they want to use.
LANGS = {
    p.stem: p.stem.capitalize()
    for p in sorted(Path(str(nltk.data.find("tokenizers/punkt"))).glob("*.pickle"))
}

# Since we're already importing Matplotlib anyway, we might as well use it to tweak the
# default visuals to something slightly nicer. For an overview of the settings, see:
# <https://matplotlib.org/stable/tutorials/introductory/customizing.html>.
matplotlib.rcParams.update({"figure.dpi": 300, "font.size": 14, "lines.markersize": 14})

app_ui = ui.page_fluid(
    ui.h1("Lexical dispersion plot"),
    ui.layout_sidebar(
        ui.panel_sidebar(
            ui.input_text(
                "words", "Words", placeholder="Space-separated words to plot"
            ),
            ui.input_text_area("text", "Text", placeholder="Text to search"),
            ui.input_selectize("lang", "Language", choices=LANGS, selected="english"),
            ui.input_text("title", "Title", value="Lexical dispersion plot"),
            ui.input_action_button("make_plot", "Make plot"),
        ),
        ui.panel_main(
            ui.output_plot("dispersion_plot"),
        ),
    ),
)


def server(input, output, session):
    @output
    @render.plot(alt="A lexical dispersion plot")
    # Only render plot when user presses the *Make plot* button, not whenever they stop
    # typing. This avoids wasting time creating plots based on incomplete input, which
    # may lead to significant delays once the user is actually done typing, because they
    # must wait for all the renders that triggered in the mean time to finish. See
    # <https://shiny.rstudio.com/py/docs/reactive-events.html> for details.
    @reactive.event(input.make_plot)
    # For the above the work, we must make this an *async* function. The details of what
    # these are are a bit complicated, but in this case, all that's needed is to add an
    # "async" before the "def" and Shiny handles the rest.
    async def dispersion_plot():
        words = []
        # Words should be space-separated, so we use .split() to get a list of strings
        # from the input string typed in by the user...
        for word in input.words().split():
            # ... but we allow for the possibility that the user didn't respect the
            # instructions and added trailing commas or colons.
            stripped_word = word.rstrip(",;")
            # In case the word consisted entirely of ,'s and ;'s, stripping them
            # resulted into an empty string. In that case, add the original (unstripped)
            # word to the list. Otherwise, add the stripped word.
            words.append(stripped_word if stripped_word else word)
        tokens = nltk.word_tokenize(input.text(), language=input.lang())
        # Don't show ugly empty plot if there's no text to search.
        if not tokens:
            return

        # Turns out that NLTK 3.8 has two bugs in its dispersion_plot code. We can fix
        # both of these by modifying the plot before returning it. To get an initial
        # idea of what you can play with and modify, I recommend taking a look at the
        # source code of nltk.draw.dispersion_plot, e.g. in a Jupyter notebook with
        # ??nltk.draw.dispersion_plot. But Matplotlib offers much beyond that; see e.g.
        # <https://matplotlib.org/cheatsheets/> as a good starting place.
        plot = nltk.draw.dispersion_plot(tokens, words)
        # Bug #1: The words are shown in the incorrect order, from bottom to top. They
        # should be reversed (from top to bottom) in order to be paired with the correct
        # dispersion plot.
        plot.set_yticks(list(range(len(words))), reversed(words), color="C0")
        # Bug #2: By default, Matplotlib autoscales the axes, which means that it zooms
        # in on the part of the plot that contains data. In the case of the y-axis, this
        # behavior is disabled because nltk.draw.dispersion_plot explicitly sets the
        # y-axis limits with plot.set_ylim(...). However, the x-axis limits are
        # autoscaled, which means that if matches are e.g. only found in the middle
        # portion of the text, the plot will zoom in on the middle. This makes it harder
        # for the user to figure out where the matches are relative to the entire text;
        # it would be more intuitive if the x-axis always spanned the full length of the
        # text, irrespective of where matches are found. We could use plot.set_xlim(...)
        # for that, but actually, we want to keep the margins added to the left and
        # right by autoscaling, as they add a little breathing room to the plot. So
        # instead, we modify the data limits to pretend the data always span the full
        # length of the text, and call plot.autoscale() to re-apply autoscaling.
        plot.dataLim.x0, plot.dataLim.x1 = 0, len(tokens) - 1
        plot.autoscale(axis="x")
        # While we're at it, we might as well tweak the x-axis label:
        plot.set_xlabel(f"Tokens (total: {len(tokens):,})")
        # And the x-axis tick labels:
        plot.xaxis.set_major_formatter(matplotlib.ticker.StrMethodFormatter("{x:,.0f}"))
        # And allow customizing the plot title:
        plot.set_title(input.title())
        return plot


app = App(app_ui, server)
